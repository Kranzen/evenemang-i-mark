var customData = require('../data/events.json');
var axios = require('axios');
var dev = true;

module.exports = {
	fetchEvents: function () {
		var url = window.encodeURI('http://localhost:5000/api/v1/events');
		var source = !dev ? url : '';
		return axios.get(source)
			.then(function(response) {
				return dev ? customData.events : response.data;
			})
			.catch(function (error) {
				console.log(error);
			});
					
	}
}