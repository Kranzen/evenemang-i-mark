import React, { Component } from 'react';
import { Switch, Route, withRouter } from 'react-router-dom'

import HomePage from './components/home/HomePage';
import EventPage from './components/event/EventPage';
import Header from './components/base/Header';
import Footer from './components/base/Footer';
import NoMatch from './components/base/NoMatch';

var api = require ('./utils/api');

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedEvent: '',
      signedInUser: {
          name: 'Anna Andersson',
          email: 'anna@andersson.test',
          role: 'user',
          category_group: {
            id: 1,
            name: 'Skogruppen',
          }
      },
      userEvents: [],
      events: [],
    }
  }

  componentDidMount() {
    this.updateEvents();
  }

  updateEvents() {
    api.fetchEvents()
    .then(function(data){
      let userEvents = data.filter((event) => {
        return event.category_group.id === this.state.signedInUser.category_group.id;
      })
      this.setState(function(){
        return {
          events: data,
          userEvents:userEvents
        }
      })
    }.bind(this));
  }

  onEventEdit = (e) => {
    console.log(e);
    this.setState({
      selectedEvent: e
    })
  }

  render() {
    return (
      <div className="App">
        <Header />
          <Switch>
            <Route exact path="/" render={(props) => ( <HomePage
              {...props}
              events={this.state.events}
              userEvents={this.state.userEvents}
              />)}
            />
            <Route path="/evenemang" render={(props) => (<EventPage
              {...props}
              userEvents={this.state.userEvents}
              selectedEvent={this.state.selectedEvent}
              onEventEdit={this.onEventEdit}
              />)}
            />
            <Route component={NoMatch} />
          </Switch>
        <Footer />
      </div>
    );
  }
}

export default withRouter(App);
