import React from 'react';
import Grid from '../base/Grid';

const Events = ({size, events}) => <Grid size={size} events={events} />

export default Events;
