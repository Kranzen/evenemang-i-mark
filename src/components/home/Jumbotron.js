import React from 'react';
import { Header, Container } from 'semantic-ui-react';
import Action from './Action';

const Jumbotron = ({title, subtitle}) => {
  return (
    <div>
      <Container text textAlign='center' className='jumbotron'>
        <div className='jumbotron_content'>
          <Header
            as='h1'
            content={title}
            inverted
          />
          <Header
            as='h2'
            content={subtitle}
            inverted
          />
          <Action />
        </div>
      </Container>
    </div>
  )
}

export default Jumbotron;
