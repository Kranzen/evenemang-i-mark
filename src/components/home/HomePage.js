import React from 'react';
import { Container, Segment } from 'semantic-ui-react';

import Jumbotron from './Jumbotron';
import UserEvents from './UserEvents';
import Events  from './Events';

const HomePage = ({events, userEvents}) => {
  return (
    <div>
      <Segment className='jumbotron' inverted>
        <Jumbotron title={'Evenemang i mark'} subtitle={'Externa och interna evenemang för Marks kommun'} />
      </Segment>
      <Container>
        <div className="event_content">
          <h2>Dina evenemang</h2>
          <UserEvents size={4} events={userEvents} />
          <h2>Alla evenemang</h2>
          <Events size={3} events={events} />
        </div>
      </Container>
    </div>
  )
}

export default HomePage;
