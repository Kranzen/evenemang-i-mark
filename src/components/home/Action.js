import React from 'react';
import { Button, Icon } from 'semantic-ui-react';
import { Link } from 'react-router-dom';

const Action = () => {
  return (
    <Link to="/evenemang">
      <Button primary size='huge' style={{marginTop: '3em' }}>
        Lägg till evenemang
        <Icon name='right arrow' />
      </Button>
    </Link>
  )
}

export default Action;
