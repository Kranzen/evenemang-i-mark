import React from 'react';
import Grid from '../base/Grid';

const UserEvents = ({size, events}) => <Grid size={size} events={events} />

export default UserEvents;
