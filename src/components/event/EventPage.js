import React from 'react';
import {
  Container,
  Grid,
  Visibility,
} from 'semantic-ui-react';

import { Route, Switch } from 'react-router-dom'

import FixedMenu from '../FixedMenu';
import VerticalMenu from '../VerticalMenu';
import CreateEvent from './CreateEvent';
import NoMatch from '../base/NoMatch';
import EditLayout from './EditLayout';

let contentAreaStyle = {
    margin: '0 2em'
}

let containerStyle = {
    marginTop: '3em',
    marginBottom: '4em'
}

export default class EventPage extends React.Component {

  constructor(props) {
		super(props);
		this.state = {
			visible: false
		}
  }

  hideFixedMenu = () => this.setState({ visible: false })
  showFixedMenu = () => this.setState({ visible: true })
/*
  showExtern = () => this.setState({ type: 'intern' })
	showIntern = () => this.setState({ type: 'extern' })
 */
  render() {
    let eventSize = this.props.userEvents.length;
    return (
      <div>
        { this.state.visible ? <FixedMenu /> : null }

        <Visibility
          onBottomPassed={this.showFixedMenu}
          onBottomVisible={this.hideFixedMenu}
          once={false}
        >
        </Visibility>

        <Container style={containerStyle}>
          <Grid>
            <Grid.Row>
              <Grid.Column width={3} only='computer'>

                <VerticalMenu match={this.props.match} eventSize={eventSize} />

              </Grid.Column>
              <Grid.Column style={contentAreaStyle} width={12}>

                <Switch>
                  <Route path={this.props.match.url + '/hantera'} render={(props) => (<EditLayout
                    match={this.props.match}
										events={this.props.userEvents}
                    selectedEvent={this.props.selectedEvent}
                    onEventEdit={this.props.onEventEdit}
                    handleEventRemove={this.props.handleEventRemove}
                    {...props}/>)}
                  />
                  <Route path='/' render={(props) => (<CreateEvent
                    {...props}
                    match={this.props.match}
                    handleEventChange={this.props.handleEventChange}
                    formData={this.props.formData}
                    errors={this.state.errors}
										onDateRemove={this.onDateRemove}
										onDateChange={this.onDateChange}/>)}
                  />
                  <Route component={NoMatch} />
                </Switch>

              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Container>
      </div>
    )
  }
}
