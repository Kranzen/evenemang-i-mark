import React from 'react';
import { Button } from 'semantic-ui-react';
import { Link } from 'react-router-dom'

const StepsNavigation = ({pos, validate, handleNextStep, handleBackStep, handleExit, match, isSubmitted, handleSubmit, isValid}) => {
	return (
		pos !== 1 && !isSubmitted ?  (
			<div>

				<Link to={isValid ? match.url + '/sammanfattning' : ''}>
					<Button floated={'right'} onClick={pos !== 3 ? validate : handleSubmit}>{pos !== 3 ? 'Nästa steg' : 'Skicka in' }</Button>
				</Link>

				<Link to={pos === 3 ? match.url + '/skapa' : match.url}>
					<Button onClick={handleBackStep} floated={'right'}>Bakåt</Button>
				</Link>

				<Link to={match.url}>
					<Button onClick={handleExit} floated={'right'}>Avbryt</Button>
				</Link>

			</div>
		) : (
			''
		)
	)
}

export default StepsNavigation
  