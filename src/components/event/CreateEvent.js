import React from 'react';
import moment from 'moment';
import { Route, Switch } from 'react-router-dom'

import EventTypeNavigation from './EventTypeNavigation'
import Form from './Form'
import Summary from './Summary'
import Steps from './Steps'
import StepsNavigation from './StepsNavigation'
import NoMatch from '../base/NoMatch'

export default class CreateEvent extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      current_date: null,
      form: {
        data: {
          title: '',
          description: '',
          date: {
            startdate: null,
            enddate: null,
            published: null,
            dates: []
          },
          location: '',
          place: '',
          price: 0,
          eventtype: '',
        },
        error: false,
        isSubmitted: false,
        isValid: false,
        errors: {}
      },
      pos: 1,
      size: 3,
      steps: {
        1: {
          isActive: true,
          isCompleted: false,
        },
        2: {
          isActive: false,
          isCompleted: false,
        },
        3: {
          isActive: false,
          isCompleted: false,
        }
      },
    }
  }

  onDateRange = (startDate, endDate) => {
    this.setState({
      form: {
        ...this.state.form,
        date: {
          ...this.state.date,
          startDate,
          endDate
        }
      }
    })
  }

  handleValidation = (e) => {
    e.preventDefault();
    const inputs = this.state.form.data;
    let errors = {};
    Object.keys(inputs).forEach(key => {
      if(inputs[key] || inputs[key] === 0) {
        if (key === 'title' || key === 'place') {
          if(!inputs[key].match(/^[a-zA-Z åäö ÅÄÖ ]*$/)){
            errors[key] = 'Du får endast ange bokstäver'
          }
        }
      } else {
        if(key === 'date') {
          inputs[key].dates.length === 0 && !inputs[key].startdate ? errors[key] = 'Du måste välja datum' : ''
        }
        errors[key] = 'Detta fält får inte vara tomt'
      }
    })

    if(Object.keys(errors).length >= 1) {
      this.setState({
        form:{
          ...this.state.form,
          errors
        }
      })
    } else {
      const pos = this.state.pos + 1;
      this.setState({
        form:{
          ...this.state.form,
          isValid: true,
          errors: {}
        },
        pos,
        steps:{
          ...this.state.steps,
          2: {isCompleted: true, isActive: false},
          3: {isCompleted: false, isActive: true}
        }
      })

      this.props.history.push(this.props.match.url + '/sammanfattning');
    }
  }

  handleSubmit = () => {
    if(!this.state.form.error) {
      this.setState({
        form: {
          ...this.state.form,
          date: {
            pusblished: moment().unix(),
          },
          isSubmitted: true,
        },
        steps: {
          ...this.state.steps,
          3: {
            isActive: false,
            isCompleted: true,
          }
        }
      })
    } else {
      this.setState({
        form: {
          ...this.state.form,
          isSubmitted: true,
        }
      })
    }
  }

  handleEventType = (eventtype) => {
    this.setState({
      form: {
        ...this.state.form,
        data: {
          ...this.state.form.data,
          eventtype
        }
      }
    })
  }

  handleEventChange = (event) => {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    const inputs = this.state.form.data;
    inputs[name] = value;

    this.setState( prevState => ({
      ...prevState,
      form: {
        ...this.state.form,
        data: inputs,
      }
      })
    )
  }

  handleChecked = (event) => {
    let checkbox_name = event.currentTarget.id;
    this.setState({
      [checkbox_name]: !this.state[checkbox_name]
    })
  }

  onFocusChange = (focused) => {
    this.setState({
      focused
    })
  }

  onDateRemove = (date) => {
    let newArr = this.state.form.data.date.dates.filter( day => {
      return moment(day).unix() !== moment(date).unix();
    })
    this.setState({
      form: {
        ...this.state.form,
        data: {
          ...this.state.form.data,
          date: {
            ...this.state.form.data.date,
            dates: newArr
          }
        }
      }
    });
  }

  onDateChange = (newdate) => {
    let duplicate = null;
    let selectedDays = this.state.form.data.date.dates;
    selectedDays.forEach(day => {
      if(moment(newdate).unix() === moment(day).unix()){
        duplicate = true;
        return;
      }
    })

    if(!duplicate){
      this.setState({
        current_date: newdate,
        form: {
          ...this.state.form,
          data: {
            ...this.state.form.data,
            date: {
              ...this.state.form.data.date,
              dates: [...this.state.form.data.date.dates, newdate]
            }
          }
        }
      });
      this.setState({
        current_date: newdate,

      });
    }
  }

  handleNextStep = () => {
    let pos = this.state.pos;
    const amount_steps = this.state.size;

    if (pos < amount_steps) {
      let currVal = pos;
      let nextVal = pos += 1;

      this.setState( prevState => ({
        ...prevState,
        pos: nextVal,
        steps: {
          ...this.state.steps,
          [currVal]: {
            isCompleted: true,
          },
          [nextVal]: {
            isActive: true,
          }
        }
        })
      )
    }

  }

  handleBackStep = () => {
    let pos = this.state.pos;
    let currVal = pos;
    let nextVal = pos -= 1;

    if (pos >= 1) {
      this.setState( prevState => ({
        ...prevState,
        pos: nextVal,
        steps: {
          ...this.state.steps,
          [currVal]: {
            isCompleted: false,
            isActive: false,
          },
          [nextVal]: {
            isActive: true,
            isCompleted: false,
          }
        }
        })
      )

    }

  }

  handleExitStep = () => {
    this.setState({
      form: {
        ...this.state.form,
        isValid: false,
        data: {
          title: '',
          description: '',
          date: {
            startdate: null,
            enddate: null,
            published: null,
            dates: []
          },
          location: '',
          place: '',
          price: 0,
          eventtype: '',
        }
      },
      pos: 1,
      steps: {
        ...this.state.steps,
        1: {
          isActive: true,
          isCompleted: false
        },
        2: {
          isActive: false,
          isCompleted: false
        },
        3: {
          isActive: false,
          isCompleted: false
        }
      }
    })
  }

  render() {
    return (
      <div>
        <Steps steps={this.state.steps} />
        <Switch>
          <Route path={this.props.match.url + '/skapa'} render={(props) => (<Form
            handleEventChange={this.handleEventChange}
            onDateChange={this.onDateChange}
            onDateRemove={this.onDateRemove}
            onDateRange={this.onDateRange}
            date={this.state.new_date}
            input={this.state.form.data}
            days={this.state.form.data.date.dates}
            FormIsValid={this.state.form.isValid}
            errors={this.state.form.errors}
            startDate={this.state.form.data.startdate}
            endDate={this.state.form.data.enddate}
            {...props}/>)}
          />
          <Route path={this.props.match.url + '/sammanfattning'} render={(props) => (<Summary
            handleEventSubmit={this.props.handleEventSubmit}
            input={this.state.form.data}
            submitted={this.state.form.isSubmitted}
            error={this.state.form.error}
            {...props}/>)}
          />
          <Route exact path={'/evenemang'} render={(props) => (<EventTypeNavigation
            match={this.props.match}
            handleNextStep={this.handleNextStep}
            onTypeChange={this.handleEventType}
            {...props}/>)}
          />
          <Route component={NoMatch} />
        </Switch>
        <StepsNavigation
          pos={this.state.pos}
          validate={this.handleValidation.bind(this)}
          handleNextStep={this.handleNextStep}
          handleBackStep={this.handleBackStep}
          handleExit={this.handleExitStep}
          match={this.props.match}
          handleSubmit={this.handleSubmit}
          isSubmitted={this.state.form.isSubmitted}
          isValid={this.state.form.isValid}
        />
      </div>
    )
  }
}
