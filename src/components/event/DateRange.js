import 'react-dates/initialize';
import "react-dates/lib/css/_datepicker.css";
import React from 'react';
import { DateRangePicker } from 'react-dates';
import moment from 'moment';
import PropTypes from 'prop-types';
import momentPropTypes from 'react-moment-proptypes';


const propTypes = {
	autoFocus: PropTypes.bool,
	autoFocusEndDate: PropTypes.bool,
	initialStartDate: momentPropTypes.momentObj,
	initialEndDate: momentPropTypes.momentObj,
};


const defaultProps = {
	disabled: false,
	required: false,
	screenReaderInputMessage: '',
	showClearDates: false,
	showDefaultInputIcon: false,
	customInputIcon: null,
	customArrowIcon: null,
	customCloseIcon: null,

	renderMonth: null,
	horizontalMargin: 0,
	withPortal: false,
	withFullScreenPortal: false,
	initialVisibleMonth: null,
	numberOfMonths: 2,
	keepOpenOnDateSelect: false,
	reopenPickerOnClearDates: false,
	isRTL: false,

	navPrev: null,
	navNext: null,
	onPrevMonthClick() {},
	onNextMonthClick() {},

	renderDay: null,
	minimumNights: 1,
	enableOutsideDays: false,
	isDayBlocked: () => false,
	isDayHighlighted: () => false,

	displayFormat: () => moment.localeData().longDateFormat('L'),
	monthFormat: 'MMMM YYYY',
	weekDayFormat: 'dd',
};

export default class DateRange extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			startDate: null,
			endDate: null,
			focusedInput: null
		}
	}

	render() {
		return (
			<DateRangePicker
				displayFormat={"YYYY-MM-DD"}
				startDate={this.state.startDate}
				endDate={this.state.endDate}
				onDatesChange={({ startDate, endDate }) => { this.setState({ startDate, endDate })}}
				focusedInput={this.state.focusedInput}
				onFocusChange={(focusedInput) => { this.setState({ focusedInput })}}
				startDatePlaceholderText="Startdatum"
				endDatePlaceholderText="Slutdatum"
				minimumNights={0}
			/>
		)
	}
}

DateRange.defaultProps = defaultProps;
