import React from 'react'
import moment from 'moment';
import { List, Label, Container, Button, Icon } from 'semantic-ui-react'

const FormMessage = ({error}) => {
  if (error) {
    return (
      <div>
        <h1>Oj, något gick fel</h1>
        <p>Försök igen eller kontakta ansvarig</p> 
      </div>
    )
  }
  return (
    <div>
      <h1>Ditt evenemang är nu inskickat</h1>
    </div>
  )
}

const FormSubmit = ({error}) => {
  return (
    <div>
      <Container textAlign='center' style={{'padding':'2em 0'}}>
        <Icon style={{'margin':'0.2em 0'}} name={error ? 'close' : 'check circle'} color={error ? 'red' : 'green'} size='massive' />
        <FormMessage error={error} />
        <Button style={{'margin':'2.5em 0'}}>OK</Button>
      </Container>
    </div>
  )
}

const FormSummary = ({input}) => {
  let dates = null;

  if(input.date.dates) {
    dates = input.date.dates.map(date => {
      return <Label key={date._d}>{moment(date).format(`YYYY / MM / DD`)}</Label>
    })
  }

  return (
    <div>
      <List divided relaxed>
        <List.Item>
          <List.Content>
            <List.Header>Rubrik</List.Header>
            <List.Description className="summary_output">{input.title}</List.Description>
          </List.Content>
        </List.Item>
        <List.Item>
          <List.Content>
            <List.Header>Beskrivning</List.Header>
            <List.Description className="summary_output">{input.description}</List.Description>
          </List.Content>
        </List.Item>
        <List.Item>
          <List.Content>
            <List.Header>Datum</List.Header>
            <List.Description className="summary_output">
              <Label.Group className="dates_group"  color='blue'>
                {dates}
              </Label.Group>
            </List.Description>
          </List.Content>
        </List.Item>
        <List.Item>
          <List.Content>
            <List.Header>Plats</List.Header>
            <List.Description className="summary_output">{input.location}</List.Description>
          </List.Content>
        </List.Item>
        <List.Item>
          <List.Content>
            <List.Header>Ort</List.Header>
            <List.Description className="summary_output">{input.place}</List.Description>
          </List.Content>
      </List.Item>
      {
        input.price > 0 ? <List.Item>
          <List.Content>
          <List.Header>Pris</List.Header>
          <List.Description className="summary_output">{input.price}</List.Description>
          </List.Content>
        </List.Item> : ''
      }
    </List>
  </div>
  )
}

const Summary = ({input, submitted, error}) => {
  console.log('Submitted: ' + submitted);
  return (
    submitted ? <FormSubmit error={error} /> : <FormSummary input={input} />
  )
}

export default Summary;