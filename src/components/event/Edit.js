import React from 'react'
import { Button, Form as UIForm} from 'semantic-ui-react'

import Form from './Form';

const EventType = () => {
  return (
    <UIForm.Field>
      <label className="form-label">Typ av evenemang</label>
      <Button>Internt evenemang</Button>
      <Button>Externt evenemang</Button>
    </UIForm.Field>
  )
}

const Edit = () => {
  return (
    <div>
      <EventType />
      <Form />
    </div>
  )
}

export default Edit;