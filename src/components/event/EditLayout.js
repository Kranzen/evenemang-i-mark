import React from 'react';
import { Route } from 'react-router-dom';
import List from './List';
import Edit from './Edit';


const EditLayout = ({events, selectedEvent, onEventEdit, match, handleEventRemove}) => {

	return (
		<div>
			<Route exact path={match.url} render={(props) => (<List
				events={events}
				onEventEdit={onEventEdit}
				match={match}
				handleEventRemove={handleEventRemove}
				{...props}/>)}
			/>
			<Route exact path={match.url + '/:id'} render={(props) => (<Edit
				selectedEvent={selectedEvent}
				{...props}/>)}
			/>
		</div>
	)
}


export default EditLayout
