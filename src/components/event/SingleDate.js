// Import dependencies
import 'react-dates/initialize';
import "react-dates/lib/css/_datepicker.css";
import React from 'react';
import { SingleDatePicker } from 'react-dates';
import moment from 'moment';
import PropTypes from 'prop-types';
import momentPropTypes from 'react-moment-proptypes';
import { Label, Icon } from 'semantic-ui-react'

const propTypes = {
  autoFocus: PropTypes.bool,
  initialDate: momentPropTypes.momentObj,
};

const defaultProps = {

  id: 'date',
  placeholder: 'Välj dag',
  disabled: false,
  required: false,
  screenReaderInputMessage: '',
  showClearDate: false,
  showDefaultInputIcon: false,
  customInputIcon: null,

  renderMonth: null,
  horizontalMargin: 0,
  withPortal: false,
  withFullScreenPortal: false,
  initialVisibleMonth: null,
  numberOfMonths: 2,
  keepOpenOnDateSelect: false,
  reopenPickerOnClearDate: false,
  isRTL: false,

  navPrev: null,
  navNext: null,
  onPrevMonthClick() {},
  onNextMonthClick() {},
  renderDay: null,
  enableOutsideDays: false,
  isDayBlocked: () => false,
  isDayHighlighted: () => {},

  displayFormat: () => moment.localeData().longDateFormat('L'),
  monthFormat: 'MMMM YYYY',
};

export default class SingleDate extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      focused: false,
    }
  }

  handleDateRemove = (day) => {
    this.props.onDateRemove(day)
  }

  handleDateChange = (day) => {
    this.props.onDateChange(day);
  }

  render = () => {
    let listOfDates = this.props.days.map(day => {
      return (
        <Label key={day._d}>
          {moment(day).format(`YYYY / MM / DD`)}
          <Icon onClick={this.handleDateRemove.bind(this, day)} name='close' />
        </Label>
      )
    })

    return (
      <div>
      <SingleDatePicker
        id="date_input"
        date={this.props.date}
        onDateChange={date => this.handleDateChange(date)}
        focused={this.state.focused}
        onFocusChange={({ focused }) => this.setState({ focused })}
      />
      <Label.Group className="dates_group" color='blue'>
        {listOfDates}
      </Label.Group>
    </div>
    )
  }
}

SingleDatePicker.defaultProps = defaultProps;
