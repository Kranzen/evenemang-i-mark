import React from 'react'
import {
  Item as UIItem
} from 'semantic-ui-react';

import Item from './Item';


const List = ({events, onEventEdit, match, handleEventRemove}) => {
  let sortedEvents = events.sort((a, b) => a.date - b.date);
  let listWithEvents = sortedEvents.map((event) => {
    return <Item key={event._id} event={event} onEventEdit={onEventEdit} match={match} handleEventRemove={handleEventRemove}/>
  })

  return (
    <UIItem.Group divided>
      {listWithEvents}
    </UIItem.Group>
  )
}

export default List;