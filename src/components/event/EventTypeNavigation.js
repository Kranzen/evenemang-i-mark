import React from 'react';
import { Link } from 'react-router-dom';
import {
	Button,
	Grid,
} from 'semantic-ui-react';

const buttonStyle = {
	margin: '0em 1.5em'
}

const gridStyle = {
	padding: '2.5em 0'
}

export default class EventTypeNavigation extends React.Component {

	handleTypeChange = (type) => {
		this.props.onTypeChange(type)
		this.props.handleNextStep()
	}

	render() {
		return (
			<Grid container style={gridStyle}>
				<Grid.Row verticalAlign={'middle'}>
					<Grid.Column textAlign={'center'}  width={8} padded={'horizontally'}>
						<h3>Externa evenemang</h3>
						<p>Om du vill skapa evenemang som kan publiceras ut mot medborgarna.</p>
						<Link to={this.props.match.url + '/skapa'}>
							<Button primary size="large" style={buttonStyle} onClick={() => (this.handleTypeChange('externt'))}>Externt evenemang</Button>
						</Link>
					</Grid.Column>
					<Grid.Column textAlign={'center'} width={8} padded={'horizontally'}>
						<h3>Interna evenemang</h3>
						<p>Om du vill skapa evenemang som endast skall visas för personalen internt på Marks kommun.</p>
						<Link to={this.props.match.url + '/skapa'}>
							<Button primary size="large" style={buttonStyle} onClick={() => (this.handleTypeChange('internt'))}>Internt evenemang</Button>
						</Link>
					</Grid.Column>
				</Grid.Row>
			</Grid>
		)
	}
}
