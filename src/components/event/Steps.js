import React from 'react';
import { Step } from 'semantic-ui-react';

const Steps = ({steps}) => {
	return (
		<Step.Group ordered>
		<Step className={steps[1].isCompleted ? 'completed' : '' || steps[1].isActive ? 'active' : 'disabled'}>
			<Step.Content>
				<Step.Title>Evenemang</Step.Title>
				<Step.Description>Välj vilken typ av evenemang</Step.Description>
			</Step.Content>
		</Step>

		<Step className={steps[2].isCompleted ? 'completed' : '' || steps[2].isActive ? 'active' : 'disabled'}>
			<Step.Content>
				<Step.Title>Beskrivning</Step.Title>
				<Step.Description>Ange information om evenemanget</Step.Description>
			</Step.Content>
		</Step>

		<Step className={steps[3].isCompleted ? 'completed' : '' || steps[3].isActive ? 'active' : 'disabled'}>
			<Step.Content>
				<Step.Title>Skicka in</Step.Title>
				<Step.Description>Granska information och skicka </Step.Description>
			</Step.Content>
		</Step>
	</Step.Group>
	)
}

export default Steps
  
  