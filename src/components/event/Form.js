// Import dependencies
import 'react-dates/initialize';
import React from 'react'
import { Form as UIForm, Checkbox, Label, TextArea } from 'semantic-ui-react'

// Import components
import DateRange from './DateRange';
import SingleDate from './SingleDate';

export default class Form extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      dateRange: false,
      singleDate: false,
    }
  }

  handleChecked = (event) => {
    let checkbox_name = event.currentTarget.id;
    this.setState({
      [checkbox_name]: !this.state[checkbox_name]
    })
  }

  render() {
    let dateRangePicker, singleDatePicker = null;
    dateRangePicker = this.state.dateRange ? <DateRange startDate={this.props.startDate} endDate={this.props.endDate} onDateRange={this.props.onDateRange} /> : '';
    singleDatePicker = this.state.singleDate ? (
      <div>
        <SingleDate
          date={this.props.date}
          days={this.props.days}
          onDateChange={this.props.onDateChange}
          onDateRemove={this.props.onDateRemove}
          onFocusChange={this.props.onFocusChange}
        />
      </div>
    ) : (
      ''
    )
    return (
      <div>
        <UIForm loading={this.state.isLoading}>
          <UIForm.Field className={this.props.errors['title'] ? 'error' : ''}>
            <label>Rubrik</label>
            <input
              name='title'
              control='input'
              value={this.props.input.title ? this.props.input.title : ''}
              onChange={this.props.handleEventChange}
            />
            {this.props.errors['title'] ? <Label basic color='red' pointing>{this.props.errors['title']}</Label> : ''} 
          </UIForm.Field>
          <UIForm.Field className={this.props.errors['description'] ? 'error' : ''}>
            <label>Beskrivning</label>
            <TextArea
              name='description'
              value={this.props.input.title ? this.props.input.description : ''}
              onChange={this.props.handleEventChange}
            />
            {this.props.errors['description'] ? <Label basic color='red' pointing>{this.props.errors['description']}</Label> : ''} 
          </UIForm.Field>
          <label className="form-label">Datum</label>
          <UIForm.Checkbox id={'singleDate'}
              checked={this.props.dayPicker}
              onClick={this.handleChecked}
              label='Specifik dag eller under en period'
          />
          <UIForm.Field id={'dateRange'}
            name='title'
            control={Checkbox}
            checked={this.props.dateRange}
            onClick={this.handleChecked}
            label='En längre period'
          />
          {dateRangePicker}
          {singleDatePicker}
          {this.props.errors['date'] ? <Label basic color='red' pointing>{this.props.errors['date']}</Label> : ''} 
          <UIForm.Field className={this.props.errors['location'] ? 'error' : ''}>
            <label>Plats</label>
            <input
              name='location'
              control='input'
              value={this.props.input.title ? this.props.input.location : ''}
              onChange={this.props.handleEventChange}
            />
            {this.props.errors['location'] ? <Label basic color='red' pointing>{this.props.errors['location']}</Label> : ''} 
          </UIForm.Field>
          <UIForm.Field className={this.props.errors['place'] ? 'error' : ''}>
            <label>Ort</label>
            <input
              name='place'
              control='input'
              value={this.props.input.title ? this.props.input.place : ''}
              onChange={this.props.handleEventChange}
            />
            {this.props.errors['place'] ? <Label basic color='red' pointing>{this.props.errors['place']}</Label> : ''} 
          </UIForm.Field>
          <UIForm.Field>
            <label>Kostnad</label>
          </UIForm.Field>
          <UIForm.Field
            control={Checkbox}
            checked={this.state.checked}
            onClick={this.handleChecked}
            label={'Besökaren måste betala för evenemanget'}
            name='price'
          />
        </UIForm>
      </div>
    )
  }
}
