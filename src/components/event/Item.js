import React from 'react';
import { Link } from 'react-router-dom'
import { 
  Button,
  Icon,
  Item as UIItem,
  Label 
} from 'semantic-ui-react'
import moment from 'moment';

const imgStyle = {
  width: '200px'
}

const Item = ({event, onEventEdit, match, handleEventRemove}) => {
  const handleClick = () => onEventEdit(event);
  let day = moment.unix(event.date.timestamp).format(`MMM DD , YYYY`);
  return (
    <UIItem>
      <UIItem.Image style={imgStyle} src={require('../../assets/img/box.jpg')} alt="image" />
      <UIItem.Content>
        <UIItem.Header>{event.title}</UIItem.Header>
        <UIItem.Meta>
          <p className="cinema">{day}</p>
          <span className='cinema'>{event.location}, {event.place}</span>
        </UIItem.Meta>
        <UIItem.Description>{event.description}</UIItem.Description>
        <UIItem.Extra>
        <Link to={match.url + '/:id'}>
        <Button onClick={handleClick} primary floated='right'>
          Redigera
          <Icon name='right chevron' />
        </Button>       
        </Link>
        <Label>{event.event_type}</Label>
      </UIItem.Extra>
      </UIItem.Content>
    </UIItem>
  )
}

export default Item
