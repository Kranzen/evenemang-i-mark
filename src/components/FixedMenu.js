import React from 'react';
import {
    Button,
    Container,
    Menu,
  } from 'semantic-ui-react';

let fixedMenuStyle = {
    background: '#1b1c1d',
    width: '100%'
}

  
let fullSize = {
    width: '100%'
  }
  
  
const FixedMenu = () => (
    <Menu style={fixedMenuStyle} fixed='top' size='large'>
      <Container>
        <Menu style={fullSize} inverted pointing secondary size='large'>
          <Menu.Item as='a' active>Hem</Menu.Item>
          <Menu.Item as='a'>Evenemang</Menu.Item>
          <Menu.Item as='a'>Kontakt</Menu.Item>
          <Menu.Item position='right'>
            <Button as='a' inverted>Logga ut</Button>
          </Menu.Item>
        </Menu>
      </Container>
    </Menu>
  )

  export default FixedMenu
  