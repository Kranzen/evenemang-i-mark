import React from 'react'
import { Link } from 'react-router-dom'
import { Input, Label, Menu } from 'semantic-ui-react'

const VerticalMenu = ({match, eventSize}) => {

  return (
    <Menu vertical className="sidebar_menu">

      <Link to="/evenemang">
        <Menu.Item name='create'>
          Skapa evenemang
        </Menu.Item>
      </Link>

      <Link to={match.url + '/hantera'}>
        <Menu.Item name='edit'>
          <Label color='teal'>{eventSize}</Label>
          Dina evenemang
        </Menu.Item>
      </Link>

      <Menu.Item>
        <Input icon='search' placeholder='Sök evenemang' />
      </Menu.Item>
    </Menu>
  )
}

export default VerticalMenu
