import React from 'react';
import {
    Button,
    Container,
    Menu,
    Segment,
    Icon
} from 'semantic-ui-react';
import { Link } from 'react-router-dom'

const Main = () => {
  return (
    <Menu className="menu_main" inverted pointing secondary size='large'>
      <Menu.Item>
        <Link to="/">Hem</Link>
      </Menu.Item>
      <Menu.Item>
        <Link to="/evenemang">Evenemang</Link>
      </Menu.Item>
      <Menu.Item position='right'>
        <Button as='a' inverted>Logga ut</Button>
      </Menu.Item>
    </Menu>
  )
}

const Mobile = () => {
  return (
    <Menu inverted secondary>
      <Menu.Item position='left'>
        <h2 style={{ 'display' : 'inline-block', 'float' : 'left'}}>EIM</h2>
      </Menu.Item>
      <Menu.Item position='right'>
        <Button inverted className="menu_mobile" floated='right'>
          <Icon name="sidebar" size='big' />
        </Button>
      </Menu.Item>
    </Menu>
  )
}

const NavBar = () => <Main />

export default NavBar;
