import React from 'react';
import { Card as CardItem } from 'semantic-ui-react';

import Card from './Card';

let eventStyle = {
  width: '100%'
}

const Grid = ({size, events}) => {

  // SORT BY DATE
  // let sortedEvents = events.sort((a, b) => a.date - b.date);

  const eventList = events.map((event) => {
    return <Card key={event._id} event={event} />
  })

  return (
    <CardItem.Group style={eventStyle} itemsPerRow={size} stackable textAlign='center'>
      {eventList}
    </CardItem.Group>
  )
}

export default Grid;
