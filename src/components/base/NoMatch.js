import React from 'react';
import { Container } from 'semantic-ui-react';

let containerStyle = {
	margin: '4em 0em',
	padding: '2em 0em'
}

const NoMatch = () => {
	return (
		<Container textAlign={'center'} style={containerStyle}>
			<h1>Oj, något gick fel</h1>
			<p>Försök igen eller kontakta ansvarig</p>
		</Container>
	)
}

export default NoMatch