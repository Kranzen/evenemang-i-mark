import React from 'react';
import NavBar from './NavBar';
import {
    Container,
    Segment,
} from 'semantic-ui-react';

const Header = () => {
    return (
      <Segment inverted textAlign='center' vertical>
        <Container>
          <NavBar />
        </Container>
      </Segment>
    )
}

export default Header
