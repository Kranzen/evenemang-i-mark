import React from 'react';
import { Card as CardItem, Image, Icon } from 'semantic-ui-react';

const Card = ({event}) => {
  return (
    <CardItem fluid>
      <Image src='' />
      <CardItem.Content textAlign='center'>
        <CardItem.Header>
          {event.title}
        </CardItem.Header>
        <CardItem.Meta>
          <span className='date'>
            {event.location},
            {event.place}
          </span>
        </CardItem.Meta>
        <CardItem.Description>
          {event.description}
        </CardItem.Description>
      </CardItem.Content>
      <CardItem.Content textAlign='center' extra>
        <a>
          <Icon name='calendar' />
          {}
        </a>
      </CardItem.Content>
    </CardItem>
  )
}

export default Card
